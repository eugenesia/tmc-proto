<?php
/**
 * @file
 * feed_importer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function feed_importer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer feeds'.
  $permissions['administer feeds'] = array(
    'name' => 'administer feeds',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'clear feed_importer feeds'.
  $permissions['clear feed_importer feeds'] = array(
    'name' => 'clear feed_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'create tmblog_feed content'.
  $permissions['create tmblog_feed content'] = array(
    'name' => 'create tmblog_feed content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any tmblog_feed content'.
  $permissions['delete any tmblog_feed content'] = array(
    'name' => 'delete any tmblog_feed content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own tmblog_feed content'.
  $permissions['delete own tmblog_feed content'] = array(
    'name' => 'delete own tmblog_feed content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any tmblog_feed content'.
  $permissions['edit any tmblog_feed content'] = array(
    'name' => 'edit any tmblog_feed content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own tmblog_feed content'.
  $permissions['edit own tmblog_feed content'] = array(
    'name' => 'edit own tmblog_feed content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'import feed_importer feeds'.
  $permissions['import feed_importer feeds'] = array(
    'name' => 'import feed_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'unlock feed_importer feeds'.
  $permissions['unlock feed_importer feeds'] = array(
    'name' => 'unlock feed_importer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
