<?php
/**
 * @file
 * feed_importer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feed_importer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feed_importer_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function feed_importer_node_info() {
  $items = array(
    'tmblog_feed' => array(
      'name' => t('tmblog feed'),
      'base' => 'node_content',
      'description' => t('To display a feed from the tmblog website'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
