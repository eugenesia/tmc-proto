<?php
/**
 * @file
 * training_calendar.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function training_calendar_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create training_sessions content'.
  $permissions['create training_sessions content'] = array(
    'name' => 'create training_sessions content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any training_sessions content'.
  $permissions['delete any training_sessions content'] = array(
    'name' => 'delete any training_sessions content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own training_sessions content'.
  $permissions['delete own training_sessions content'] = array(
    'name' => 'delete own training_sessions content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any training_sessions content'.
  $permissions['edit any training_sessions content'] = array(
    'name' => 'edit any training_sessions content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own training_sessions content'.
  $permissions['edit own training_sessions content'] = array(
    'name' => 'edit own training_sessions content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
