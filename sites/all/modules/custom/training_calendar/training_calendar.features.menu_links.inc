<?php
/**
 * @file
 * training_calendar.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function training_calendar_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_training:training/month
  $menu_links['main-menu_training:training/month'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'training/month',
    'router_path' => 'training/month',
    'link_title' => 'Training',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_training:training/month',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Training');


  return $menu_links;
}
