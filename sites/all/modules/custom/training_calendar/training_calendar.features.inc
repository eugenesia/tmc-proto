<?php
/**
 * @file
 * training_calendar.features.inc
 */

/**
 * Implements hook_views_api().
 */
function training_calendar_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function training_calendar_node_info() {
  $items = array(
    'training_sessions' => array(
      'name' => t('Training Sessions'),
      'base' => 'node_content',
      'description' => t('To store training sessions entries'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
